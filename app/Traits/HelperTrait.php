<?php

namespace App\Traits;

trait HelperTrait
{
    /**
     * Remove duplicate values in array
     * @param array $arr
     * @param string $glue
     * @return mixed array/string
     */
    public function removeDuplicatesInArray(array $array, $glue = null)
    {
        $result = array_unique($this->removeEmptyArrayElements($array));
        if (!$this->isEmpty($glue)) {
            $result = $this->implodeArray($glue, $result);
        }
        return $result;
    }

    /**
     * Combine the values of an array using a glue
     * @param string $glue
     * @param array $array
     * @return array
     */
    public function implodeArray(string $glue, array $array)
    {
        return array_reduce($array, function ($carry, $item) use ($glue) {
            return !$carry ? $item : ($carry . $glue . $item);
        });
    }

    public function removeEmptyArrayElements($array)
    {
        return array_values(array_filter($array));
    }
}
