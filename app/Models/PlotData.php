<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlotData extends Model
{
    use HasFactory;

    protected $table = 'plot_data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['wkt_geom', 'lotID', 'X', 'Y'];
}
