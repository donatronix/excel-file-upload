<?php

namespace App\Imports;

use App\Models\PlotData;
use Maatwebsite\Excel\Concerns\ToModel;

class PlotDataImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new PlotData([
            'wkt_geom' => $row[0], 'lotID' => $row[1], 'X' => $row[2], 'Y' => $row[3]
        ]);
    }
}
