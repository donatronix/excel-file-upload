<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePlotDataRequest;
use App\Http\Requests\UpdatePlotDataRequest;
use App\Imports\PlotDataImport;
use App\Models\PlotData;
use App\Traits\HelperTrait;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class PlotDataController extends Controller
{
    use HelperTrait;

    /**
     * @return \Illuminate\Support\Collection
     */
    public function import(StorePlotDataRequest $request)
    {
        DB::beginTransaction();
        try {
            Excel::import(new PlotDataImport, $request->file('file')->store('temp'));
            PlotData::where('wkt_geom', 'wkt_geom')->delete();
        } catch (\Throwable $th) {
            DB::rollback();
            return back()->withErrors($th->getMessage(), 'error');
        }
        DB::commit();
        return back();
    }
}
